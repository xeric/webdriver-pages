(ns webdriver-pages.core-test
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [webdriver-pages.core :refer :all]))

(use-fixtures :once (fn [f]
                      (set-driver! {:browser :firefox})
                      (f)
                      (quit)))

(defpage google-page []
  :url          "http://www.google.com"
  :exists?      :query-field
  :query-field  "input[name='q']"
  :submit-query "input[name='btnK']"
  :submit-sm    "button[name='btnG']")

(defpage google-search-page []
  :exists?      :github-link
  :github-link  (fn []
                  (find-element
                   {:xpath "//a[contains(.,'semperos/clj-webdriver')]"}))
  :search-entry (fn [t] (find-element {:tag :a :text t})))

(deftest search-google
  (goto google-page)
  (at google-page
      (input-text :query-field "clj-webdriver")
      (if (present? :submit-query)
        (click :submit-query)
        (click :submit-sm)))
  (is (at? google-search-page))
  (at google-search-page
      (click [:search-entry "Taxi API Documentation"])))

  
