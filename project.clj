(defproject xeric/webdriver-pages "0.0.1-SNAPSHOT"
  :description
  "Integration testing library (around clj-webdriver) using page object patterns"
  :dependencies
  [[org.clojure/clojure "1.8.0"]
   [clj-webdriver "0.7.2"]]
  :source-paths ["src"]
  :profiles
  {:dev
   {:dependencies
    [[org.seleniumhq.selenium/selenium-java "2.52.0"]
     [org.seleniumhq.selenium/selenium-api "2.52.0"]
     [org.seleniumhq.selenium/selenium-server "2.52.0"]
     [org.seleniumhq.selenium/selenium-firefox-driver "2.52.0"]
     [org.seleniumhq.selenium/selenium-htmlunit-driver "2.52.0"]
     [org.seleniumhq.selenium/selenium-remote-driver "2.52.0"]]}}
  :test-paths ["test" "test-resources"])
