# webdriver-pages
A page object pattern library for [clj-webdriver](https://github.com/semperos/clj-webdriver)

## Installation
 `[xeric/webdriver-pages "0.01-SNAPSHOT"]`
 
## Usage

Start by importing clj-webdriver and webdriver-pages 

````clojure
(ns my.namespace
  (:require [clj-webdriver.core :refer :all]
            [webdriver-pages.core :refer :all]))
````

Next create a page containing selectors for a given page

````clojure
; A top menu shared across pages
(def top-menu
  {:search-btn    ".clj-nav-search"
   :query-textbox "#q"})

; Clojure home page
(defpage clojure-home-page [top-menu]
  :url "http://clojure.org"
  :exists? :intro-message
  :intro-message ".clj-intro-message")

(defpage clojure-search-results [top-menu]
  :exists? :search-results
  :search-results ".clj-search-results")
  
````

Now you can start having fun with clj-webdriver 

````clojure
(goto clojure-home-page)
(at clojure-home-page
  (click :search-btn)
  (wait-until #(displayed? :query-textbox)
  (input-text :query-text "transducers")
  (click)))
(is (at? clojure-search-results))
````




