(ns webdriver-pages.core
  "### Page object pattern for testing with clj-webdriver
  This is a wrapper around clj-webdriver library which is itself an excellent
  wrapper around webdriver.

  Each page/screen is defined as a clojure map.
  e.g.

      (defpage google-search
        :at \"http://google.com\"
        :exists? :name
        :name    \"input[name='q']\")
 
  then in code you can interact with the page as

      (goto google-search)
      (at google-search
        (click :search))
      (at google-results)"
  (:require [clj-webdriver.taxi :as t]
            [clj-webdriver.element :refer [element-like? element?]]))

(def ^{:dynamic true :private true} *page*)
(def ^{:dynamic true :private true} *outer-finder*)

(defn- page-selector?
  [q]
  (or (keyword? q) (vector? q)))

(declare page-finder)

(defn- run-outside-page
  [f args]
  (binding [t/*finder-fn* *outer-finder*]
    (if-let [r (apply f args)]
      (list r))))

(defprotocol IFindSelector
  (find-selector [this]))

(extend-protocol IFindSelector
  Object
  (find-selector [this] this)
  clojure.lang.Keyword
  (find-selector [this]
    (if (contains? *page* this)
      (get *page* this)
      (throw (ex-info (str this ": key not found") {}))))
  clojure.lang.PersistentVector
  (find-selector [[q & args]]
    (let [f (find-selector q)]
      (if (fn? f)
        (apply f args)
        (throw (ex-info (str "When vector is used as key "
                             "the first parameter must be "
                             "a keyword") {}))))))

(defn- elements-under
  ([p q] (elements-under t/*driver* p q))
  ([driver p q]
     (let [sel (find-selector q)
           sel (if (map? sel) sel {:css sel})]
       (t/find-elements-under p sel))))

(defn- by-vector [qs]
  (->> (reduce (fn [result q]
                 (let [sel (find-selector q)]
                   (conj result sel)))
               [] qs)
       (interpose " ")
       (apply str)
       (find-selector)
       (*outer-finder*)))

(defn page-finder
  ([q] (page-finder t/*driver* q))
  ([driver q]
   (cond
     (element-like? q) q
     (some? q)
     (let [finder *outer-finder*
           sel    (find-selector q)]
       (cond
         (element-like? sel) (list sel)
         (fn? sel)           (if-let [result (sel)]
                               (list result))
         (vector? sel)       (by-vector sel)
         (nil? sel)          nil
         :else               (finder sel))))))

(defn page-exists?
  [{:keys [exists?] :as page}]
  {:pre [(some? (:exists? page))]}
  (if (fn? exists?)
    (exists?)
    (t/exists? exists?)))

(defmacro at
  "Checks if `page` is the current page in the browser and 
   defines a page context. These contexts cannot be nested.
   e.g.

       (at some-page
         (click :next-btn))"
  [page & forms]
  `(binding [*page* ~page
             *outer-finder* t/*finder-fn*
             t/*finder-fn* page-finder]
     (t/wait-until #(page-exists? ~page))
     ~@forms))

(defn at?
  "Checks if `page` is the current page in the browser."
  [page]
  `(binding [*page* ~page
             *outer-finder* t/*finder-fn*
             t/*finder-fn* page-finder]
     (t/wait-until #(page-exists? ~page))))
 
(defn goto
  "Go to a given `page`. Page must have a key `:url` pointing to the
   url of the page"
  [page]
  {:pre [(some? (:url page))]}
  (t/to (:url page)))

(defmacro defpage
  "Defines a page as a set of key and value pairs. Each key is a 
   keyword which is used as a selector in a page context. Page
   contexts are defined using `at`. 
   Each page must have atleast a member `:exists?`

   A selector can be a key in a page or a vector in which case
   the first parameter is the key pointing to a function and the
   rest of the elements are parameters to that function.

       (defn next-btn [i]
         (nth (elements \"button.next\") i))

       (defpage :some-page
         :next-btn next-btn)

       (at some-page
         (click [:next-btn 0]))"
  [name args & {:as elements}]
  `(def ~name (merge ~@args ~elements)))


